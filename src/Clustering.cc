#include "Clustering.h"
#include "geo.h"

#include <iostream>
#include <algorithm>

#include <TH1F.h>
#include <TH2F.h>

//____________________________________________________________________________________________________
Clustering::Clustering( const config& c, const std::string& _filename, const unsigned& num )
  : RawAnalysis( c, "Clustering", num, _filename )
{}

Clustering::~Clustering(){
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}


//____________________________________________________________________________________________________
void Clustering::init() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  std::string maskmap_filename = cfg[ana_type][AnaID::HotPixelAnalysis]["HotPixelAnalysis"]["output"];
  
  if( verbosity ) std::cout << maskmap_filename << std::endl;
  maskmap_file = TFile::Open( maskmap_filename.c_str() );
  
  if( verbosity>= debug ) {
    maskmap_file->ls();
  }
  for( const auto& plane: geo::planes ) {
    maskmaps[plane] = dynamic_cast<TH2F*>( maskmap_file->Get( Form("hitmap_pl%u_maskmap", plane) ) );
    if( !maskmaps[plane] ) {
      throw( Form("ERROR! maskmap plane %u is null!", plane ) );
    }
  }
  
  initTree();
  
  for( const auto& plane : geo::planes ) {
    hitdata[plane] = new std::vector<hit>;
    clusterdata[plane] = new std::vector<cluster*>;
    dist_nclus[plane] = new TH1F(Form("num_cluster_pl%d", plane), ";Number of Clusters", 200, 0, 200);
    dist_size[plane] = new TH1F(Form("cluster_size_pl%d", plane), ";Cluster Size", 40, 0, 40);
    dist_sumToT[plane] = new TH1F(Form("cluster_sumToT_pl%d", plane), ";Cluster Sum ToT [BC]", 100, 0, 100);
  }
  
  // output tree
  std::string ofilename = cfg[ana_type][order][ana_name]["output"];
  ofile = new TFile( ofilename.c_str(), "recreate");
  otree = new TTree("clusterTree", "clusterTree");
  otree->Branch( "plane", &out_plane );
  otree->Branch( "size", &out_size );
  otree->Branch( "ave_col", &out_ave_col );
  otree->Branch( "ave_row", &out_ave_row );
  otree->Branch( "sumToT", &out_sumToT );
  
  n_passed = 0;
}

//____________________________________________________________________________________________________
void Clustering::clear() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  for( auto pair : clusterdata ) {
    if( verbosity>=verbose ) std::cout << pair.first << " " << pair.second << pair.second->size() << std::endl;
    for( auto* cluster : *pair.second ) delete cluster;
    pair.second->clear();
  }
  
  for( auto pair : hitdata ) {
    if( verbosity>=verbose ) std::cout << pair.first << " " << pair.second << pair.second->size() << std::endl;
    pair.second->clear();
  }
  
  out_plane.clear();
  out_size.clear();
  out_ave_col.clear();
  out_ave_row.clear();
  out_sumToT.clear();
  
}

//____________________________________________________________________________________________________
void Clustering::print() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  for( auto pair: clusterdata ) {
    std::cout << Form("Plane [%2u] : number of clusters = %lu", pair.first, pair.second->size()) << std::endl;
    for( auto *cluster : *pair.second ) {
      std::cout << Form("   Cluster size = %u, sumToT = %.0f, ave_col=%.2f, ave_row=%.2f",
                        cluster->size, cluster->sumToT, cluster->ave_col, cluster->ave_row )
                << std::endl;
    }
  }
}


//____________________________________________________________________________________________________
void Clustering::worker(const unsigned& plane, Clustering* c ) {
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << " plane " << plane << std::endl;
  auto *pre_pre_clusters = new std::vector<cluster*>;
  c->gen_clusters_row( c->hitdata[plane], pre_pre_clusters );
  
  if( verbosity ) std::cout << " row-clustered: " << pre_pre_clusters->size() << std::endl;
  
  for( auto *pre_pre_cluster : *pre_pre_clusters ) {
    auto *pre_clusters = new std::vector<cluster*>;
    c->gen_clusters_col( pre_pre_cluster->hits, pre_clusters );
    
    if( verbosity ) std::cout << "  -> col-clustered: " << pre_clusters->size() << std::endl;
    for( auto *pre_cluster : *pre_clusters ) {
      c->gen_clusters( pre_cluster->hits, c->clusterdata[plane] );
      delete pre_cluster;
    }
    delete pre_clusters;
    
    delete pre_pre_cluster;
  }
  delete pre_pre_clusters;
  
  if( verbosity ) std::cout << "   --> final clusters: " << c->clusterdata[plane]->size() << std::endl;
  return;
}

//____________________________________________________________________________________________________
void Clustering::processEntry() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  clear();
  
  // collect non-noisy hits 
  for(auto ihit=0; ihit<data.size(); ihit++) {
    //if( verbosity>= verbose ) std::cout << "hit " << ihit << std::endl;
    const auto& hit = data.get_hit( ihit );
    
    auto *maskmap = maskmaps[ hit->iden ];
    if( 0 == maskmap->GetBinContent( hit->col+1, hit->row+1 ) ) {
      hitdata.at( hit->iden )->emplace_back( data.get_hit_new( ihit ) );
    }
  }
  
  for( auto& plane : geo::planes ) {
    worker(plane, this);
  }
  
  if( verbosity ) print();
  
  // Filling the cluster information
  for( auto pair: clusterdata ) {
    dist_nclus  [pair.first]->Fill( pair.second->size() );
    for( auto *cluster : *pair.second ) {
      dist_size  [pair.first]->Fill( cluster->size );
      dist_sumToT[pair.first]->Fill( cluster->sumToT );
    }
  }
  
  
  for( auto pair: clusterdata ) {
    auto& plane   = pair.first;
    auto* clusters = pair.second;
    
    for( auto* cluster : *clusters ) {
      out_plane   .emplace_back( plane );
      out_size    .emplace_back( cluster->size );
      out_ave_col .emplace_back( cluster->ave_col );
      out_ave_row .emplace_back( cluster->ave_row );
      out_sumToT  .emplace_back( cluster->sumToT );
    }
  }
  
  otree->Fill();
  
  n_passed++;
  
  clear();
}


//____________________________________________________________________________________________________
void Clustering::gen_clusters_col( std::vector<hit>* hits, std::vector<cluster*>* clusters ) {
  
  if( 0 == hits->size() ) return;
  
  std::sort( hits->begin(), hits->end() );
  
  //if( verbosity ) std::cout << __PRETTY_FUNCTION__ << ": number of hits = " << hits->size() << std::endl;
  
  auto theCol = hits->at(0).col;
  
  // initial cluster
  auto* clus = new cluster;
  
  for( auto& hit : *hits ) {
    if( hit.col > theCol + 1 ) {
      clusters->emplace_back( clus );
      clus = new cluster;
    }
    clus->hits->emplace_back( hit );
    theCol = hit.col;
  }
  clusters->emplace_back( clus );
  
  
}


//____________________________________________________________________________________________________
void Clustering::gen_clusters_row( std::vector<hit>* hits, std::vector<cluster*>* clusters ) {
  
  // Assuming that the hit array is sorted in row.
  
  if( 0 == hits->size() ) return;
  
  std::sort( hits->begin(), hits->end() );
  
  auto theRow = hits->at(0).row;
  
  // initial cluster
  auto* clus = new cluster;
  
  for( auto& hit : *hits ) {
    if( hit.row > theRow + 1 ) {
      clusters->emplace_back( clus );
      clus = new cluster;
    }
    clus->hits->emplace_back( hit );
    theRow = hit.row;
  }
  clusters->emplace_back( clus );
  
}


//____________________________________________________________________________________________________
void Clustering::gen_clusters( std::vector<hit>* hits, std::vector<cluster*>* clusters ) {
  
  ////////////////////////////////////////////////////////////////
  //
  // First. Create a single-hit cluster for each hit
  //
  std::vector<cluster*> cs;
  
  for( auto& hit : *hits ) {
    
    auto *clus = new cluster;
    clus->hits->emplace_back( hit );
    cs.emplace_back( clus );
  }
  
  
  ////////////////////////////////////////////////////////////////
  //
  // Second. merge clusters
  //
  while( true ) {
    
    bool merge_flag = false;
    
    // loop over product combinations of clusters
    for( auto i = 0; i<cs.size(); i++ ) {
      for( auto j = i+1; j<cs.size(); j++ ) {
        
        const auto *iclus = cs.at(i);
        const auto *jclus = cs.at(j);
        
        // check if there are nearby hits between (ihit in iclus) and (jhit in jclus)
        
        for( int ii = iclus->hits->size()-1; ii>=0; ii-- ) {
          for( int jj = 0; jj < jclus->hits->size(); jj++ ) {
            
            const auto& ihit = iclus->hits->at(ii);
            const auto& jhit = jclus->hits->at(jj);
            
            if( ( (ihit.col == jhit.col)        and abs( ihit.row - jhit.row ) <= 1 ) or
                ( abs(ihit.col - jhit.col) <= 1 and (ihit.row == jhit.row) )        ) {
              
              merge_flag = true;
              break;
              
            }
          }
          if( merge_flag ) break;
        }
        
        if( merge_flag ) {
          // merge jhits into iclus
          for( auto& jhit : *jclus->hits ) {
            iclus->hits->emplace_back( jhit );
          }
          
          // erase jclus from the list
          cs.erase( std::find(cs.begin(), cs.end(), jclus) );
          
          // delete the instance jclus
          delete jclus;
          
          break;
        }
        
      }
      if( merge_flag ) break;
    }
    
    // If no clusters are not able to merge, break.
    if( !merge_flag ) break;
  }
  
  
  ////////////////////////////////////////////////////////////////
  //
  // Third. Calculate cluster properties
  //
  for( auto *cluster : cs ) {
    cluster->sumToT  = 0.0;
    cluster->ave_col = 0.0;
    cluster->ave_row = 0.0;
    for( auto& hit: *cluster->hits ) {
      cluster->sumToT += hit.tot;
      cluster->ave_col += hit.col * hit.tot;
      cluster->ave_row += hit.row * hit.tot;
    }
    cluster->ave_col /= cluster->sumToT;
    cluster->ave_row /= cluster->sumToT;
    cluster->size = cluster->hits->size();
    
    clusters->emplace_back( cluster );
  }
  
  
}

//____________________________________________________________________________________________________
void Clustering::end() {
  
  
  for( const auto& plane : geo::planes ) {
    dist_nclus[plane]  ->Write();
    dist_size[plane]   ->Write();
    dist_sumToT[plane] ->Write();
  }
  
  ofile->Write();
  ofile->Close();
  
  maskmap_file->Close();
  
}



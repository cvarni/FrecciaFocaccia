#include "Alignment.h"
#include "Sensor.h"
#include "geo.h"

#include <TH1F.h>
#include <TF1.h>
#include <TF2.h>
#include <TProfile.h>
#include <TProfile2D.h>

#include <fstream>
#include <algorithm>

//____________________________________________________________________________________________________
Alignment::Alignment( const config& c, const std::string& _filename, const unsigned& num )
  : Tracking( c, "Alignment", _filename, num )
{
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}


//____________________________________________________________________________________________________
Alignment::~Alignment() {}


//____________________________________________________________________________________________________
void Alignment::init() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  initTree();
  loadInputs();
  initMonitoring();
  
  if( cfg["alignment"].find( "nevents_max") != cfg["alignment"].end() ) {
    nEntries_override = cfg["alignment"]["nevents_max"];
  }
  
}

//____________________________________________________________________________________________________
void Alignment::end() {
  
  // Track fitting and calculate residuals
  fit_tracks();
  
  // Fill histograms
  for( const auto& trk : track_accum ) {
    if( trk->status == track::fit_bad ) continue;
    
    for( const auto& meas : (*trk).measurements ) {
      auto& plane = (*meas).plane;
      resx[plane]->Fill( (*meas).residual.X() );
      resy[plane]->Fill( (*meas).residual.Y() );
    }
    
    track_chi2_dist_pre->Fill( (*trk).chi2 );
    track_chi2_dist_log_pre->Fill( log10( (*trk).chi2) );
  }
  
  const unsigned nmax= cfg["alignment"]["ntracks_max"];
  
  do_align(nmax);
  
  summary2();
  
}

//____________________________________________________________________________________________________
void Alignment::do_align_single( const unsigned& plane, const std::string level, const int& nmax) {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  const auto& align_cfg = cfg["alignment"];
  
  auto& geom = geo::geometry[plane];
  
  
  std::map<std::string, enum geo::params> dofmap;
  dofmap["Tx"] = geo::Tx;
  dofmap["Ty"] = geo::Ty;
  dofmap["Tz"] = geo::Tz;
  dofmap["Rx"] = geo::Rx;
  dofmap["Ry"] = geo::Ry;
  dofmap["Rz"] = geo::Rz;
  dofmap["Sx"] = geo::Sx;
  dofmap["Sy"] = geo::Sy;
  
  ROOT::Fit::Fitter fitter;
  
  auto *pFitter = new PlaneFitter<Fitter::kTrans>
    (cfg, track_accum, plane,
     align_cfg["chi2_cut"],
     align_cfg["residual_cut"],
     nmax                       );
  
  
  ROOT::Math::Functor fcn( *pFitter, geo::ndof );
  
  double pStart[geo::ndof];
  pStart[geo::Tx] = geom.Trans.X();
  pStart[geo::Ty] = geom.Trans.Y();
  pStart[geo::Tz] = geom.Trans.Z();
  pStart[geo::Rx] = geom.Rot.X();
  pStart[geo::Ry] = geom.Rot.Y();
  pStart[geo::Rz] = geom.Rot.Z();
  pStart[geo::Sx] = geom.Scale.X();
  pStart[geo::Sy] = geom.Scale.Y();
  
  fitter.SetFCN(fcn, pStart);
  
  for (int i = 0; i < geo::ndof; ++i) {
    fitter.Config().ParSettings(i).SetStepSize(1.e-7);
    fitter.Config().ParSettings(i).Fix();
  }
  
  for( auto dof : align_cfg["levels"][level] ) {
    fitter.Config().ParSettings( dofmap[dof] ).Release();
  }
  
  for( const auto& m : align_cfg["minimizer"] ) {
    
    std::cerr << Form("plane %2u align %s: ", plane, level.c_str() ) << std::flush;
    
    const std::string minimizer = m;
    fitter.Config().SetMinimizer( "Minuit", minimizer.c_str() );
    fitter.SetFCN(fcn, pStart);
    bool ok = fitter.FitFCN();
    
    if( !ok ) {
      std::cerr << Form("%-10s failed.", minimizer.c_str() ) << std::endl;
      continue;
    }
    
    const auto nHits = pFitter->nhits;
    
    const ROOT::Fit::FitResult & result = fitter.Result();
    std::cout << Form("success (%s). %7u hits used. Chi2/Ndof = %.3f, ncall = %u", 
                      minimizer.c_str(), nHits, result.MinFcnValue(), result.NCalls() ) << std::endl;
    
    const double * params = result.GetParams();
    const double * errors = result.GetErrors();
    
    geom.Trans.SetX ( params[geo::Tx] );
    geom.Trans.SetY ( params[geo::Ty] );
    geom.Trans.SetZ ( params[geo::Tz] );
    geom.Rot.SetX   ( params[geo::Rx] );
    geom.Rot.SetY   ( params[geo::Ry] );
    geom.Rot.SetZ   ( params[geo::Rz] );
    geom.Scale.Set  ( params[geo::Sx], params[geo::Sy] );
    
    geom.Trans_err.SetX ( errors[geo::Tx] );
    geom.Trans_err.SetY ( errors[geo::Ty] );
    geom.Trans_err.SetZ ( errors[geo::Tz] );
    geom.Rot_err.SetX   ( errors[geo::Rx] );
    geom.Rot_err.SetY   ( errors[geo::Ry] );
    geom.Rot_err.SetZ   ( errors[geo::Rz] );
    geom.Scale_err.Set  ( errors[geo::Sx], errors[geo::Sy] );
    
    break;
  }
  
  delete pFitter;
}


//____________________________________________________________________________________________________
void Alignment::do_align(const int nmax) {
  
  const auto& align_cfg = cfg["alignment"];
  
  if( !align_cfg["enabled"] ) return;
  
  std::cout << "\n----------------------------" << std::endl;
  std::cout << "Sequential single alignment" << std::endl;
  
  auto levels = align_cfg["levels"];
  unsigned int level_index =0;
  for( auto it = levels.begin(); it != levels.end(); ++it ) {
    
    const std::string level = it.key();
    
    std::cout << "\n----------------------------" << std::endl;
    std::cout << "Alignment " << level << ": used DoF = " << std::flush;
    for( auto dof : align_cfg["levels"][level] ) {
      std::cout << dof << ", " << std::flush;
    }
    std::cout << std::endl;
    
    summary1();
    
    for( auto& plane : geo::planes ) {
      
      if( geo::origin_plane == plane ) continue;
      
      fit_tracks( nmax, plane );
      
      do_align_single( plane, level, nmax );
      
    }
    
    // Refitting
    fit_tracks( nmax );
    
    std::cout << std::endl;
    
    level_index++;
  }
  
  for( auto iplane=0; iplane<geo::planes.size(); iplane++) {
    const auto& plane = geo::planes.at(iplane);
    const auto& geom = geo::geometry[plane];
    
    align_Tx->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Ty->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Rx->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Ry->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Rz->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Sx->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    align_Sy->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    
    
    align_Tx->SetBinContent( iplane+1, geom.Trans.X() );
    align_Ty->SetBinContent( iplane+1, geom.Trans.Y() );
    align_Rx->SetBinContent( iplane+1, geom.Rot.X() );
    align_Ry->SetBinContent( iplane+1, geom.Rot.Y() );
    align_Rz->SetBinContent( iplane+1, geom.Rot.Z() );
    align_Sx->SetBinContent( iplane+1, geom.Scale.X() );
    align_Sy->SetBinContent( iplane+1, geom.Scale.Y() );
    
    align_Tx->SetBinError( iplane+1, geom.Trans_err.X() );
    align_Ty->SetBinError( iplane+1, geom.Trans_err.Y() );
    align_Rx->SetBinError( iplane+1, geom.Rot_err.X() );
    align_Ry->SetBinError( iplane+1, geom.Rot_err.Y() );
    align_Rz->SetBinError( iplane+1, geom.Rot_err.Z() );
    align_Sx->SetBinError( iplane+1, geom.Scale_err.X() );
    align_Sy->SetBinError( iplane+1, geom.Scale_err.Y() );
    
  }
  
  std::cout << "----------------------------\n" << std::endl;
  
  // Error scale tuning
  if( cfg["alignment"]["enable_error_scale_adjustment"] ) {
    
    for( unsigned iplane = 0; iplane < geo::planes.size(); iplane++ ) {
      
      const unsigned& plane = geo::planes.at(iplane);
      error_scale_tune( plane, nmax );
      
      const auto& geom = geo::geometry[plane];
      
    }
    
  }
  
  for( unsigned iplane = 0; iplane < geo::planes.size(); iplane++ ) {
    
    const unsigned& plane = geo::planes.at(iplane);
    const auto& geom = geo::geometry[plane];
    
    pullx_byPlanes->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    pully_byPlanes->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    pullx_byPlanes->SetBinContent( iplane+1, pullx_aligned[plane]->GetRMS() );
    pully_byPlanes->SetBinContent( iplane+1, pully_aligned[plane]->GetRMS() );
    
    error_scale_x->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    error_scale_y->GetXaxis()->SetBinLabel(iplane+1, Form("%u", plane ));
    error_scale_x->SetBinContent( iplane+1, geom.error_scale.X() );
    error_scale_y->SetBinContent( iplane+1, geom.error_scale.Y() );
  }
  
  config align_out;
  for( auto& plane : geo::planes ) {
    
    auto& geom = geo::geometry[plane];
    std::cout << Form("Plane %2u: \n"
                      "  trans    = (%9.2e +/- %8.2e, %9.2e +/- %8.2e),\n"
                      "  rot      = (%9.2e +/- %8.2e, %9.2e +/- %8.2e, %9.2e +/- %9.2e),\n"
                      "  scale    = (%9.2e +/- %8.2e, %9.2e +/- %8.2e)\n"
                      "  errScale = (%9.2e, %9.2e)",
                      plane,
                      geom.Trans.X(), geom.Trans_err.X(), geom.Trans.Y(), geom.Trans_err.Y(),
                      geom.Rot  .X(), geom.Rot_err  .X(), geom.Rot  .Y(), geom.Rot_err  .Y(), geom.Rot.Z(), geom.Rot_err.Z(),
                      geom.Scale.X(), geom.Scale_err.X(), geom.Scale.Y(), geom.Scale_err.Y(),
                      geom.error_scale.X(), geom.error_scale.Y()
                      )
              << std::endl;
    
    align_out["geometry_fine_alignment"][Form("%u", plane)]
      = { { "prealign"    , { geom.pre_align.X(), geom.pre_align.Y()         } },
          { "trans"       , { geom.Trans.X(), geom.Trans.Y(), geom.Trans.Z() } },
          { "rot"         , { geom.Rot.X(),   geom.Rot.Y(),   geom.Rot.Z()   } },
          { "scale"       , { geom.Scale.X(), geom.Scale.Y()                 } },
          { "error_scale" , { geom.error_scale.X(), geom.error_scale.Y()     } }   };
    
  }
  
  // Write out the new alignment result
  const std::string filename = cfg["alignment"]["output"];
  std::ofstream ofs( filename.c_str() );
  ofs << align_out.dump(4) << std::endl;
  ofs.close();
  
  std::cout << "----------------------------\n" << std::endl;
  
}


//____________________________________________________________________________________________________
void Alignment::error_scale_tune( const unsigned& plane, const int& nmax, bool do_tune ) {
  
  auto& geom = geo::geometry[plane];
  
  unsigned ntracks = 0;
  for( auto& trk : track_accum ) {
    if( trk->status == track::fit_bad ) continue;
    
    ntracks++;
    
    if( nmax>0 and ntracks > nmax ) break;
  }
  
  auto pullx = pullx_aligned[plane];
  auto pully = pully_aligned[plane];
  
  const auto esx_new = pullx->GetRMS() * geom.error_scale.X();
  const auto esy_new = pully->GetRMS() * geom.error_scale.Y();
  
  if( do_tune ) {
    geom.error_scale.Set( esx_new, esy_new );
    std::cout << Form("Error Scale tuned: plane %2u : new error scale = (%.3f, %.3f)", plane, geom.error_scale.X(), geom.error_scale.Y() ) << std::endl;
  }
  
  fit_tracks( nmax );
  
}


#include "OptParser.h"
#include <iostream>

options::options(const int argc, const char** argv) {
  int i = 0;
  
  
  optTags =
    {
      std::make_tuple( std::string("--config"),   options::cfg,     std::string("[config_file]") ),
      std::make_tuple( std::string("--analysis"), options::ana,     std::string("[analysis_type] available: RawAnalysis, ClusterAnalysis, TrackAnalysis") ),
      std::make_tuple( std::string("--input"),    options::input,   std::string("[input_file]") ),
      std::make_tuple( std::string("--help"),     options::help,    std::string("Print helps") ),
      std::make_tuple( std::string("--verbose"),  options::verbose, std::string("verbose level. 0 = muted (default), 1 = info, 2 = debug, 3 = verbose") ),
      std::make_tuple( std::string("--batch"),    options::verbose, std::string("optimized printout for batch mode") ),
      std::make_tuple( std::string("-c"),         options::cfg,     std::string("same as --config") ),
      std::make_tuple( std::string("-a"),         options::ana,     std::string("same as --analysis") ),
      std::make_tuple( std::string("-i"),         options::input,   std::string("same as --input") ),
      std::make_tuple( std::string("-h"),         options::help,    std::string("same as --help") ),
      std::make_tuple( std::string("-v"),         options::verbose, std::string("same as --verbose") ),
      std::make_tuple( std::string("-b"),         options::verbose, std::string("same as --batch") )
    };
  
  unsigned state = options::idle;
  
  bool help_flag = false;
  
  while( i < argc ) {
    
    const std::string str = argv[i];
    
    //std::cout << "state = " << state << ", str = " << str << std::endl;
    
    bool flag = false;
    
    switch( state ) {
    case options::idle:
      for( auto& tuple : optTags ) {
        if( str == std::get<options::opt>( tuple ) ) {
          state = std::get<options::mode>( tuple );
          
          if( str == "-v" or str == "--verbose" ) strings[options::verbose] = "1";
          if( str == "-b" or str == "--batch"   ) strings[options::batch]   = "1";
          break;
        }
      }
      i++;
      break;
      
    case options::help:
      print_help();
      help_flag = true;
      break;
      
    case options::verbose:
      for( auto& tuple : optTags ) {
        if( str == std::get<options::opt>( tuple ) ) {
          state = options::idle;
          flag = true;
          break;
        }
      }
      if( !flag ) {
        strings[state] = str;
        state = options::idle;
        i++;
      }
      break;
      
    case options::batch:
      for( auto& tuple : optTags ) {
        if( str == std::get<options::opt>( tuple ) ) {
          state = std::get<options::mode>( tuple );
          flag = true;
          break;
        }
      }
      if( !flag ) {
        strings[state] = str;
        state = options::idle;
        i++;
      }
      break;
      
    default:
      if( "=" == str ) break;
      for( auto& tuple : optTags ) {
        if( str == std::get<options::opt>( tuple ) ) {
          print_help();
          help_flag = true;
          throw( "Invalid options");
        }
      }
      strings[state] = str;
      state = options::idle;
      
      i++;
      
      break;
    }
    
    if( help_flag ) break;
  }
  
  if( !help_flag ) {
    if( strings[options::cfg]   == "" ) { print_help(); throw( "Error! No config file specified!" );   }
    if( strings[options::ana]   == "" ) { print_help(); throw( "Error! No analysis type specified!" ); }
    if( strings[options::input] == "" ) { print_help(); throw( "Error! No input file specified!" );    }
  } else {
    throw("");
  }
  
}

void options::print() {
  std::map<unsigned, std::string> item_name {
      { options::cfg,     "Config file" }
    , { options::ana,     "Analysis mode" }
    , { options::ana,     "Analysis mode" }
    , { options::input,   "Input file" }
    , { options::verbose, "Verbosity level" }
    , { options::batch,   "Batch mode" }
  }; 
  
  std::cout << "\nConfigurations:\n" << std::endl;
  for( auto& pair : strings ) {
    printf(" - %-20s: %s\n", item_name[pair.first].c_str(), strings[pair.first].c_str() );
  }
  std::cout << "\n-----------------------------------------------------------" << std::endl;
}

void options::print_help() {
  std::cout << "focaccia: a light-weight telescope data analyser.\n" << std::endl;
  for( auto& tuple : optTags ) {
    printf("  %-10s : %s\n", std::get<options::opt>(tuple).c_str(), std::get<options::descr>(tuple).c_str() );
  }
}

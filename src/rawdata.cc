#include "rawdata.h"
#include <iostream>

rawdata::rawdata() {
  iden = new std::vector<int>;
  col = new std::vector<int>;
  row = new std::vector<int>;
  tot = new std::vector<int>;
  lv1 = new std::vector<int>;
  m_hit = new hit;
};

rawdata::~rawdata() {
  delete lv1;
  delete tot;
  delete row;
  delete col;
  delete iden;
}

bool operator<( const hit& h1, const hit& h2 ) noexcept {
  if( h1.row == h2.row ) {
    return ( h1.col < h2.col );
  } else {
    return h1.row < h2.row;
  }
}


void rawdata::clear() {
  iden->clear();
  col->clear();
  row->clear();
  tot->clear();
  lv1->clear();
}

hit* rawdata::get_hit( const unsigned& ihit ) noexcept {
  if( ihit < iden->size() ) {
    m_hit->iden = iden->at( ihit );
    m_hit->row  = row->at( ihit );
    m_hit->col  = col->at( ihit );
    m_hit->tot  = tot->at( ihit );
    m_hit->lv1  = lv1->at( ihit );
  } else {
    std::cerr << __PRETTY_FUNCTION__ << "ERROR: specified index out of the range" << std::endl;
  }
  return m_hit;
}

hit rawdata::get_hit_new( const unsigned& ihit ) noexcept {
  hit h;
  if( ihit < iden->size() ) {
    h.iden = iden->at( ihit );
    h.row  = row->at( ihit );
    h.col  = col->at( ihit );
    h.tot  = tot->at( ihit );
    h.lv1  = lv1->at( ihit );
  } else {
    std::cerr << __PRETTY_FUNCTION__ << "ERROR: specified index out of the range" << std::endl;
  }
  return h;
}

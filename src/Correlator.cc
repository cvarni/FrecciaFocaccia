#include "Correlator.h"
#include "HistoBank.h"
#include "geo.h"

#include <TVector3.h>
#include <TH1F.h>
#include <TH2F.h>

//____________________________________________________________________________________________________
class Correlator_impl: public HistoBank {
public:
  Correlator_impl();
  ~Correlator_impl() {}
  bool cut( const cluster& c ) const;
  void write();
  
  using h1_holder     = std::map<int, std::shared_ptr<TH1F> >;
  using h2_holder     = std::map<int, std::shared_ptr<TH2F> >;
  using h2_holder_raw = std::map<int, TH2F* >;
  
  std::unique_ptr<TFile> maskmap_file;
  
  h2_holder_raw maskmaps;
  
  h2_holder xymaps;
  h2_holder xxcorrs;
  h2_holder yycorrs;
  h2_holder xycorrs;
  h1_holder xxdiffs;
  h1_holder yydiffs;
  
  unsigned long long nEntries;
  
};

//____________________________________________________________________________________________________
Correlator::Correlator( const config& c, const std::string& _filename, const unsigned& num )
  : ClusterAnalysis( c, "Correlator", _filename, num )
  , m_impl( new Correlator_impl )
{}

Correlator::~Correlator(){}




//____________________________________________________________________________________________________
void Correlator::init() {
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  initTree();
  
  std::string maskfilename = cfg[ana_type][0]["MaskMap"]["output"];
  
  m_impl->maskmap_file = std::unique_ptr<TFile>( TFile::Open( maskfilename.c_str() ) );
  if( !m_impl->maskmap_file->IsOpen() ) {
    throw( "ERROR! maskmap file not open!" );
  }
  
  for( const auto& plane: geo::planes ) {
    m_impl->maskmaps[plane] = dynamic_cast<TH2F*>( m_impl->maskmap_file->Get( Form("xymap_pl%u_mask", plane) ) );
    if( !m_impl->maskmaps[plane] ) {
      throw( Form("ERROR! maskmap plane %u is null!", plane ) );
    }
  }
  
}



//____________________________________________________________________________________________________
void Correlator::processEntry() {
  
  if( m_impl->nEntries >= 40000 ) return;
  
  for( unsigned iclus=0; iclus<data.num_clus(); iclus++) {
    const auto& cluster = data.get( iclus );
    
    if( m_impl->cut( cluster ) ) continue;
    
    const TVector3 pos = get_global_pos( cluster, geo::prealign::wo_prealign );
    
    m_impl->xymaps[ cluster.plane ]->Fill( pos.X(), pos.Y() );
  }
  
  for( unsigned iclus=0; iclus<data.num_clus(); iclus++) {
    const auto& icluster = data.get( iclus );
    if( icluster.plane != geo::origin_plane ) continue;
    
    if( m_impl->cut( icluster ) ) continue;
    
    for( unsigned jclus=iclus+1; jclus<data.num_clus(); jclus++) {
      const auto& jcluster = data.get( jclus );
      
      if( jcluster.plane == geo::origin_plane ) continue;
      
      if( m_impl->cut( jcluster ) ) continue;
      
      const TVector3 ipos = get_global_pos( icluster, geo::prealign::wo_prealign );
      const TVector3 jpos = get_global_pos( jcluster, geo::prealign::wo_prealign );
      
      m_impl->xxcorrs[jcluster.plane]->Fill( ipos.X(), jpos.X() );
      m_impl->yycorrs[jcluster.plane]->Fill( ipos.Y(), jpos.Y() );
      
      m_impl->xxdiffs[jcluster.plane]->Fill( jpos.X() - ipos.X() );
      m_impl->yydiffs[jcluster.plane]->Fill( jpos.Y() - ipos.Y() );
      
    }
  }
  
  m_impl->nEntries++;
}

void Correlator::end() {
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  
  std::string outfilename = cfg[ana_type][order][ana_name]["output"];
  TFile *ofile = new TFile( outfilename.c_str(), "recreate");
  m_impl->write();
  ofile->Close();
  
}



//____________________________________________________________________________________________________
Correlator_impl::Correlator_impl()
  : maskmap_file( nullptr )
  , nEntries( 0 )
{
  for( const auto& plane : geo::planes ) {
    xymaps[plane] = allocate<TH2F>(Form("xymap_pl%d", plane), ";x [mm];y [mm]", 400, -20, 20, 400, -20, 20 );
    
    if( plane == geo::origin_plane ) continue;
    xxcorrs[plane] = allocate<TH2F>( Form("xxcorr_pl0_pl%u", plane), ";x [mm];x [mm]", 400, -20, 20, 400, -20, 20 );
    yycorrs[plane] = allocate<TH2F>( Form("yycorr_pl0_pl%u", plane), ";y [mm];y [mm]", 400, -20, 20, 400, -20, 20 );
    xxdiffs[plane] = allocate<TH1F>( Form("xxdiff_pl0_pl%u", plane), ";x [mm];#Deltax [mm]", 2000, -20, 20 );
    yydiffs[plane] = allocate<TH1F>( Form("yydiff_pl0_pl%u", plane), ";y [mm];#Deltay [mm]", 2000, -20, 20 );
  }
}


//____________________________________________________________________________________________________
void Correlator_impl::write() {
  for( const auto& plane : geo::planes ) {
    xymaps[plane]->Write();
    
    if( plane == geo::origin_plane ) continue;
    xxcorrs[plane]->Write();
    yycorrs[plane]->Write();
    xxdiffs[plane]->Write();
    yydiffs[plane]->Write();
  }
  
}

//____________________________________________________________________________________________________
bool Correlator_impl::cut( const cluster& c ) const {
  bool flag = false;
  TVector3 pos = ClusterAnalysis::get_global_pos( c, geo::prealign::wo_prealign );
  if( maskmaps.at(c.plane)
      ->GetBinContent( maskmaps.at(c.plane)->GetXaxis()->FindBin( pos.X() ),
                       maskmaps.at(c.plane)->GetXaxis()->FindBin( pos.Y() ) ) > 0 ) {
    flag = true;
  }
  return flag;
}


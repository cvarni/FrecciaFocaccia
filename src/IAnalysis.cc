#include "IAnalysis.h"
#include <iostream>
#include <chrono>
#include <cmath>

#include "geo.h"


int IAnalysis::verbosity = IAnalysis::muted;
bool IAnalysis::batch     = false;
std::deque< std::unique_ptr<IAnalysis> > IAnalysis::ana_queue;

std::mutex IAnalysis::mtx;

//____________________________________________________________________________________________________
IAnalysis::IAnalysis( const config& c, const std::string& name, const std::string& type, const unsigned& num )
  : cfg( c )
  , ana_name( name )
  , ana_type( type )
  , order( num )
  , file( nullptr )
  , tree( nullptr )
  , nEntries_override( -1 )
{
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  geo::init( c );
};

IAnalysis::~IAnalysis() {
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

//____________________________________________________________________________________________________
void IAnalysis::processAll() {
  init();
  loop();
  end();
}

//____________________________________________________________________________________________________
void IAnalysis::loop() {
  
  Long64_t nEntries = tree->GetEntries();
  
  if( cfg["global"]["max_event"] != -1 ) {
    nEntries = cfg["global"]["max_event"];
  }
  
  if( nEntries_override > 0 and nEntries > nEntries_override ) {
    nEntries = nEntries_override;
  }
  
  if( verbosity >= verbose ) {
    nEntries = 1;
  }
  
  std::chrono::system_clock::time_point tstart, tend;
  
  tstart = std::chrono::system_clock::now();
  
  double expected_endtime = 3600;
  
  const unsigned v = verbosity;
  
  for(Long64_t ientry = 0; ientry< nEntries; ientry++) {
    if( verbosity>=debug and ientry == 0 ) {
      verbosity = verbose;
    }
    
    if( ientry> nEntries*0.01 and ientry%100 == 99 ) {
      tend = std::chrono::system_clock::now();
      double elapsed = std::chrono::duration_cast<std::chrono::seconds>(tend - tstart).count();
      
      expected_endtime = std::round( (nEntries-ientry)*1.0/(ientry*1.0) * elapsed );
    }
    
    if( !batch ) {
      if( ientry%(nEntries/1000) == 17 ) {
        
        if( expected_endtime > 60. ) {
          std::cout << Form(" [ %-20s ]: Entry = %6llu / %6llu (%5.1f%%) : Expected %2d min %2.0f sec to finish...      \r",
                            ana_name.c_str(), ientry, nEntries,
                            (ientry*100.0)/(nEntries*1.0),
                            (int)(expected_endtime/60.),
                            expected_endtime-(int)(expected_endtime/60.)*60.0 )
                    << std::flush;
        } else {
          std::cout << Form(" [ %-20s ]: Entry = %6llu / %6llu (%5.1f%%) : Expected %.0f sec to finish...           \r",
                            ana_name.c_str(), ientry, nEntries,
                            (ientry*100.0)/(nEntries*1.0),
                            expected_endtime )
                    << std::flush;
        }
      }
    } else {
      if( ientry%(nEntries/50) == 0 ) {
        std::cout << "." << std::flush;
      }
    }
    
    tree->GetEntry(ientry);
    processEntry();
    
    if( ientry == 0 ) {
      verbosity = muted;
    }
  }
  
  verbosity = v;
  
  tend = std::chrono::system_clock::now();
  double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tend - tstart).count();
  if( batch ) std::cout << std::endl;
  std::cout << Form(" [ %-20s ]: Entry = %6llu / %6llu (%5.1f%%) : Done. %.2f sec in total                         ",
                    ana_name.c_str(), nEntries, nEntries, (nEntries*100.0)/(nEntries*1.0), elapsed*1.e-3 ) << std::endl;
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << " end." << std::endl;
}


#include "EudaqConv.h"
#include "HotPixelAnalysis.h"
#include "Clustering.h"
#include "MaskMap.h"
#include "Correlator.h"
#include "Alignment.h"
#include "Tracking.h"
#include "TrackAnaExample.h"

//____________________________________________________________________________________________________
void IAnalysis::play( const config& config,
                      const std::string& ana_type,
                      const std::string& input_file )
{
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << " start." << std::endl;
  
  
  using gen = std::unique_ptr<IAnalysis> (*)( const nlohmann::json&, const std::string&, const unsigned& );
  std::map<std::string, gen> ana_switch {
    { "EudaqConv",        IAnalysis::gen_analysis<EudaqConv>        },
    { "HotPixelAnalysis", IAnalysis::gen_analysis<HotPixelAnalysis> },
    { "Clustering",       IAnalysis::gen_analysis<Clustering>       },
    { "MaskMap",          IAnalysis::gen_analysis<MaskMap>          },
    { "Correlator",       IAnalysis::gen_analysis<Correlator>       },
    { "Alignment",        IAnalysis::gen_analysis<Alignment>        },
    { "Tracking",         IAnalysis::gen_analysis<Tracking>         },
    { "TrackAnaExample",  IAnalysis::gen_analysis<TrackAnaExample>  }
  };
  
  
  unsigned count = 0;
  
  if( config.find( ana_type ) == config.end() ) {
    throw(Form("Analysis type %s not found in the config!", ana_type.c_str()));
  }
  
  const auto& cfg = config[ana_type];
  
  std::cout << Form("\n%s: Registered Analyses:\n", ana_type.c_str() ) << std::endl;
  
  // Loop over each analysis
  for( auto& o : cfg ) {
    for( auto it = o.begin(); it != o.end(); ++it ) {
      std::string ana_name = it.key();
      bool enabled = it.value()["enabled"];
      std::cout << Form(" [%s] %s", enabled? "X" : " ", ana_name.c_str() ) << std::endl;
    }
  }
  std::cout << "\n-----------------------------------------------------------\n" << std::endl;
  
  
  // Loop over each analysis
  unsigned id = 0;
  for( auto& o : cfg ) {
    for( auto it = o.begin(); it != o.end(); ++it ) {
      
      std::string ana_name = it.key();
      bool enabled = it.value()["enabled"];
      
      if( enabled ) {
        
        if( ana_switch.find( ana_name ) == ana_switch.end() ) {
          throw( Form("%s is not registered in the Analysis list.", ana_name.c_str() ) );
        }
        
        auto ana = ana_switch[ana_name]( config, input_file, id );
        ana->processAll();
      }
      
      id++;
    }
  }
  
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << " end." << std::endl;
}

#include "EudaqConv.h"
#include "geo.h"

#include <iostream>

EudaqConv::EudaqConv( const config& c, const std::string& fname, const unsigned& num )
  : IAnalysis( c, "EudaqConv", "EudaqConv", num )
  , filename( fname )
{}

EudaqConv::~EudaqConv(){
  if( verbosity ) std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void EudaqConv::initTree() {
  file = std::unique_ptr<TFile>( TFile::Open( filename.c_str() ) );
  if( !file->IsOpen() ) throw( "File is not open!" );
  
  tree = dynamic_cast<TTree*>( file->Get("tree") );
  if( !tree ) throw( "Tree is not available. Maybe wrong input file specified?" );
  
  tree->SetBranchAddress("id_plane",     &eudata.id_plane     );
  tree->SetBranchAddress("id_hit",       &eudata.id_hit       );
  tree->SetBranchAddress("id_x",         &eudata.id_x         );
  tree->SetBranchAddress("id_y",         &eudata.id_y         );
  tree->SetBranchAddress("id_frame",     &eudata.id_frame     );
  tree->SetBranchAddress("id_frame",     &eudata.id_frame     );
  tree->SetBranchAddress("id_charge",    &eudata.id_charge    );
  tree->SetBranchAddress("id_pivot",     &eudata.id_pivot     );
  tree->SetBranchAddress("i_time_stamp", &eudata.i_time_stamp );
  tree->SetBranchAddress("i_tlu",        &eudata.i_tlu        );
  tree->SetBranchAddress("i_run",        &eudata.i_run        );
  tree->SetBranchAddress("i_event",      &eudata.i_event      );
}



void EudaqConv::init() {
  
  std::string ofilename = cfg[ana_type][order][ana_name]["output"];
  ofile = std::unique_ptr<TFile>( TFile::Open( ofilename.c_str(), "recreate" ) );
  
  otree = new TTree("tree", "tree");
  otree->Branch("euEvt", &(raw.euEvt) );
  otree->Branch("iden",  &(raw.iden)  );
  otree->Branch("row",   &(raw.row)   );
  otree->Branch("col",   &(raw.col)   );
  otree->Branch("tot",   &(raw.tot)   );
  otree->Branch("lv1",   &(raw.lv1)   );
  
  initTree();
  
}

void EudaqConv::processEntry() {
  
  if( event_number != eudata.i_event ) {
    
    if( event_number != -1 ) {
      otree->Fill();
    }
    
    raw.euEvt    = eudata.i_event;
    event_number = eudata.i_event;
    
    raw.clear();
    
  }
  
  const auto& geom = geo::geometry[eudata.id_plane];
  
  raw.iden->emplace_back( eudata.id_plane );
  raw.col ->emplace_back( geom.sensor_col( eudata.id_x ) );
  raw.row ->emplace_back( geom.sensor_row( eudata.id_y ) );
  raw.tot ->emplace_back( eudata.id_charge );
  raw.lv1 ->emplace_back( eudata.id_frame );
  
}

void EudaqConv::end() {
  
  ofile->Write();
  ofile->Close();
  
}



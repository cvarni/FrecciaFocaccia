#ifndef __TrackData__
#define __TrackData__

#include <vector>

typedef struct hitdata {
  int    plane;
  bool   is_hit;
  double x;
  double y;
  double folded_x;
  double folded_y;
  int    trkcol;
  int    trkrow;
  double local_x;
  double local_y;
  int    size;
  double sumToT;
  double ave_col;
  double ave_row;
  double res_x;
  double res_y;
} hitdata;
  
class trackvars {
public:
  double chi2;
  int nhits;
  int nhits_tel;
  std::vector<int>*    plane;
  std::vector<double>* x;
  std::vector<double>* y;
  std::vector<double>* folded_x;
  std::vector<double>* folded_y;
  std::vector<int>* trkcol;
  std::vector<int>* trkrow;
  std::vector<double>* local_x;
  std::vector<double>* local_y;
  std::vector<int>*    size;
  std::vector<double>* sumToT;
  std::vector<double>* ave_col;
  std::vector<double>* ave_row;
  std::vector<double>* res_x;
  std::vector<double>* res_y;
  hitdata d;

  trackvars();
  ~trackvars();
  
  const hitdata& data( const unsigned& ipl );
};
  
#endif

#ifndef __ALIGNFITTER_DETAILS__
#define __ALIGNFITTER_DETAILS__

#include "alignfitter.h"


template<unsigned int T>
unsigned PlaneFitter<T>::nhits {0};

template<unsigned int T>
PlaneFitter<T>::PlaneFitter( const config& c, std::vector< std::shared_ptr<track> >& _tracks, const unsigned& _pl, double _cut, double _res_cut, const int& max )
  : cfg( c ), tracks( _tracks ), pl( _pl ), chi2_cut( _cut ), res_cut( _res_cut ), nmax( max )
{}


//____________________________________________________________________________________________________
template<unsigned int T>
void PlaneFitter<T>::chi2_perTrack( const std::shared_ptr<track>& trk, const double* par, const unsigned index, PlaneFitter<T>* ptr ) {
  
  auto sqr = []( const double& v1 ) -> double { return v1 * v1; };
  auto sumsqr = [&sqr]( const double& v1, const double& v2 ) -> double { return sqr(v1) + sqr(v2); };
  
  const auto& geom = geo::geometry[ptr->pl];
  
  for( auto& meas : trk->measurements ) {
    auto& plane = meas->plane;
    
    if( plane != ptr->pl ) continue;
    
    // Here, hit0 is the position before updating alignment
    TVector3 hit0 = meas->hit;  geom.apply_alignment( hit0 );
    
    // This vector determines the size of the lever arm for rotation
    // Ensure the lever arm is 2-dim vector.
    TVector3 lever = meas->hit; lever.SetZ(0.0);
    
    const TVector3 trk_pos = trk->get_global( plane );
    
    // Some rough criterion to reject outlier hits
    if( (hit0 - trk_pos).Perp() > ptr->res_cut ) continue;
    
    // Apply alignment using parameters under fitting
    double scale_x = par[geo::Sx];
    double scale_y = par[geo::Sy];
    
    // Here, hit1 is the position using the parameters under fitting. Apply custom alignment.
    TVector3 hit1 = meas->hit;
    
    // Scaling. Plust set zpos = 0 before applying rotations
    hit1.SetXYZ( hit1.x()*(1.0+scale_x), hit1.y()*(1.0+scale_y), 0.0 );
    
    double rotx = par[geo::Rx];
    double roty = par[geo::Ry];
    double rotz = par[geo::Rz];
    
    hit1.RotateX( rotx );
    hit1.RotateY( roty );
    hit1.RotateZ( rotz );
    
    // Translation + putting back the z position
    TVector3 trans( par[geo::Tx], par[geo::Ty], par[geo::Tz] );
    
    hit1 += trans;
    
    //--------------------------------------------------------------
    // Now the custom alignment for hit1 is over. Calculate chi2.
    
    // Residual vectors and errors. Ensure residual is 2-dim vector.
    TVector3 res = hit1 - trk_pos; res.SetZ(0);
    TVector3 err = geom.global_err();
    
    // Chi2 for translation
    const double& tr = sumsqr( res.x()/err.x(), res.y()/err.y() );
    
    // Chi2 for rotation. Use the lever vector
    double prod = ( lever.x()*res.y() - lever.y()*res.x() ) / ( err.x() * err.y() );
    
    // prod_ref is the normalised lever arm length
    double prod_ref = sumsqr( lever.x()/err.x(), lever.y()/err.y() );
    
    const double& rot = sqr( prod );
    
    const double& inner_prod = lever.x()*res.x()/err.x() + lever.y()*res.y()/err.y();
    
    // put a weight on the track depending on the eccentricity of the track position
    // from the center of the plane, divided by track chi2
    //const double weight = sqr(hit.Perp() / trk->chi2);
    
    ptr->chi2_trans      .at(index) = tr;
    ptr->chi2_trans_norm .at(index) = 2;
    ptr->chi2_rot        .at(index) = rot;
    ptr->chi2_rot_norm   .at(index) = prod_ref;
    ptr->chi2_scale      .at(index) = sqr( inner_prod );
    ptr->chi2_scale_norm .at(index) = sqr( lever.Perp() );
    
  }
}



//____________________________________________________________________________________________________
template<unsigned int T>
inline const double PlaneFitter<T>::get_chi2() const { return 0.0; }

template<>
inline const double PlaneFitter<Fitter::strategy::kTrans>::get_chi2() const {
  
  const double trans_sum      = std::accumulate( chi2_trans.begin(),        chi2_trans.end(),      0.0 );
  const double trans_norm_sum = std::accumulate( chi2_trans_norm.begin(),   chi2_trans_norm.end(), 0.0 );
  
  return trans_sum/trans_norm_sum;
}

template<>
inline const double PlaneFitter<Fitter::strategy::kTransRot>::get_chi2() const {
  
  const double trans_sum      = std::accumulate( chi2_trans.begin(),        chi2_trans.end(),      0.0 );
  const double trans_norm_sum = std::accumulate( chi2_trans_norm.begin(),   chi2_trans_norm.end(), 0.0 );
  const double rot_sum        = std::accumulate( chi2_rot.begin(),          chi2_rot.end(),        0.0 );
  const double rot_norm_sum   = std::accumulate( chi2_rot_norm.begin(),     chi2_rot_norm.end(),   0.0 );
  
  return ( trans_sum/trans_norm_sum + rot_sum/rot_norm_sum );
}

template<>
inline const double PlaneFitter<Fitter::strategy::kTransRotScale>::get_chi2() const {
  
  const double trans_sum      = std::accumulate( chi2_trans.begin(),        chi2_trans.end(),      0.0 );
  const double trans_norm_sum = std::accumulate( chi2_trans_norm.begin(),   chi2_trans_norm.end(), 0.0 );
  const double rot_sum        = std::accumulate( chi2_rot.begin(),          chi2_rot.end(),        0.0 );
  const double rot_norm_sum   = std::accumulate( chi2_rot_norm.begin(),     chi2_rot_norm.end(),   0.0 );
  const double scale_sum      = std::accumulate( chi2_scale.begin(),        chi2_scale.end(),      0.0 );
  const double scale_norm_sum = std::accumulate( chi2_scale_norm.begin(),   chi2_scale_norm.end(), 0.0 );
  
  return ( trans_sum/trans_norm_sum + rot_sum/rot_norm_sum + scale_sum/scale_norm_sum );
}


//____________________________________________________________________________________________________
template<unsigned int T>
double PlaneFitter<T>::operator() (const double *par) {
  
  nhits = 0;
  
  chi2_trans      .clear();
  chi2_trans_norm .clear();
  chi2_rot        .clear();
  chi2_rot_norm   .clear();
  chi2_scale      .clear();
  chi2_scale_norm .clear();
  
  std::vector<std::thread> threads;
  
  const int nCore = cfg["global"]["thread_cores"];
  
  for( auto& trk : tracks ) {
    chi2_trans      .emplace_back( 0.0 );
    chi2_trans_norm .emplace_back( 0.0 );
    chi2_rot        .emplace_back( 0.0 );
    chi2_rot_norm   .emplace_back( 0.0 );
    chi2_scale      .emplace_back( 0.0 );
    chi2_scale_norm .emplace_back( 0.0 );
  }
  
  for( const auto& trk : tracks ) {
    
    if( (*trk).status == track::fit_bad ) continue;
    
    if( (*trk).chi2 > chi2_cut ) continue;
    
    const auto& geom = geo::geometry[pl];
    
    bool flag = false;
    for( auto& meas : (*trk).measurements ) {
      auto& plane = (*meas).plane;
      if( plane == pl ) {
        flag = true;
        break;
      }
    }
    
    if( flag ) {
      threads.emplace_back( std::thread( chi2_perTrack, trk, par, nhits, this ) );
      nhits++;
    }
    
    if( nmax > 0 and nhits >= nmax ) break;
    
    if( 0 == nhits % nCore ) {
      for( auto& th : threads ) th.join();
      threads.clear();
    }
    
  }
  
  for( auto& th : threads ) th.join();
  threads.clear();
  
  return get_chi2();
  
}

#endif

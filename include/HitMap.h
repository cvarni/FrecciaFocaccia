#ifndef __HitMap__
#define __HitMap__

#include "Sensor.h"
#include <TH2F.h>
#include <TMath.h>

class IHitMap {
public:
  virtual ~IHitMap() noexcept {}
  virtual TH1F *get_distribution    (const int& nbins, const double norm = 1.0) = 0;
  virtual TH1F *get_distribution_log(const int& nbins, const double norm = 1.0) = 0;
};

template<FE type>
class HitMap final : public TH2F, public IHitMap {
 public:
  HitMap(const char* name, const char* title=";Column;Row");
  ~HitMap() noexcept {}
  TH1F *get_distribution    (const int& nbins, const double norm = 1.0) override;
  TH1F *get_distribution_log(const int& nbins, const double norm = 1.0) override;
};

#include "details/HitMap_details.h"

#endif

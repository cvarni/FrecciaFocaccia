#ifndef __RawAnalysis__
#define __RawAnalysis__

#include "IAnalysis.h"
#include "rawdata.h"
#include <TFile.h>
#include <TTree.h>

class RawAnalysis : public IAnalysis {
protected:
  
  rawdata data;
  std::string filename;
  
  void initTree() override;
  
public:
  RawAnalysis( const config& c, const std::string& name, const unsigned& id, const std::string& filename );
  virtual ~RawAnalysis();
};


#endif

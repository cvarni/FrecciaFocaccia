#!/bin/sh

demoname=Genova

if [ $# -ge 1 ]; then
    demoname=$1
fi

input=""
configFile=""

if [ "${demoname}" == "Genova" ] ; then
    input=eudaq_conv_004522.root
    configFile=config/GenovaAug2016.json
    wget https://www.dropbox.com/s/jky6bll1k559zp3/${input} -p ${outDir}
elif [ "${demoname}" == "Barcelona" ] ; then
    input=eudaq_conv_007429.root
    configFile=config/BarcelonaOct2016.json    
    wget https://www.dropbox.com/s/08jgkddqnaa1hmo/${input} -p ${outDir}
else
    echo "Demo options are either \"Genova\" or \"Barcelona\"."
    exit 1
fi


make -j

outDir=${PWD}/output
mkdir -p ${outDir}

./bin/focaccia -c ${configFile} -a EudaqConv       -i ${outDir}/${input}
./bin/focaccia -c ${configFile} -a RawAnalysis     -i ${outDir}/raw.root
./bin/focaccia -c ${configFile} -a ClusterAnalysis -i ${outDir}/clusters.root
./bin/focaccia -c ${configFile} -a TrackAnalysis   -i ${outDir}/tracks.root



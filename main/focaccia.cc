#include "IAnalysis.h"
#include "Opening.h"
#include "OptParser.h"

#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <chrono>

int main(const int argc, const char** argv) {

  std::chrono::system_clock::time_point tstart, tend;
  tstart = std::chrono::system_clock::now();
  
  const std::string version = "0.2";

  try {

    options opt( argc, argv );

    IAnalysis::batch = opt.find<options::batch>();
    opening(version, IAnalysis::batch);
    
    opt.print();

    std::ifstream config_file( opt.string<options::cfg>() );
    config cfg;
    config_file >> cfg;
    config_file.close();

    
    if( opt.find<options::verbose>() ) {
      IAnalysis::verbosity = std::stoi( opt.string<options::verbose>() );
    }

    IAnalysis::play( cfg, opt.string<options::ana>(), opt.string<options::input>() );

    
  } catch( const char* msg ) {
    std::cout << "\n\n==========================================\n" << std::endl;
    std::cout << "EXCEPTION CAUGHT" << std::endl;
    std::cerr << msg << std::endl;
    std::cout << "\n\nFocaccia terminated irregularly." << std::endl;
    return 1;
  }

  tend = std::chrono::system_clock::now();
  double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tend - tstart).count();
  ending(elapsed);
  
  return 0;
}
    
